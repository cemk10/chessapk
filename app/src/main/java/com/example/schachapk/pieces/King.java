package com.example.schachapk.pieces;

import com.example.schachapk.common.Pair;

public class King extends Piece{
    public King(Pair startPosition, PieceColor startColor) {
        super(startPosition, startColor);
    }
    public String getText (){
        if (getColor() == PieceColor.BLACK){
            return "Schwarzer König"; }
        else {
            return "Weißer König";}
    }
}
