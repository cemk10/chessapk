package com.example.schachapk.pieces;

import com.example.schachapk.common.Pair;

public class Bishop extends Piece {
    public Bishop(Pair startPosition, PieceColor startColor) {
        super(startPosition, startColor);
    }
    public String getText (){
        if (getColor() == PieceColor.BLACK){
            return "Schwarzer Läufer"; }
        else {
            return "Weißer Läufer";}
    }
}
