package com.example.schachapk.pieces;


import com.example.schachapk.common.Pair;

public abstract class Piece {
    private Pair position;
    private PieceColor color;
    public Piece(Pair startPosition, PieceColor startColor){
        position = startPosition;
        color = startColor;

    }

    public Pair getPosition() {
        return position;

    }
    public void setPosition(Pair newPosition) {
        position = newPosition;

    }
    public PieceColor getColor(){
        return color;

    }
    public abstract String getText();
}
