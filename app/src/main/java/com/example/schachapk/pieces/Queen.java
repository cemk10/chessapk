package com.example.schachapk.pieces;

import com.example.schachapk.common.Pair;

public class Queen extends Piece {


    public Queen(Pair startPosition, PieceColor startColor) {
        super(startPosition,startColor);
    }
    public String getText (){
        if (getColor() == PieceColor.BLACK){
            return "Schwarze Dame"; }
        else {
            return "Weiße Dame";}
        }
    }

