package com.example.schachapk.pieces;

import com.example.schachapk.common.Pair;

public class Knight  extends Piece {
    public Knight (Pair startPosition, PieceColor startColor) {
        super(startPosition, startColor);
    }
    public String getText (){
        if (getColor() == PieceColor.BLACK){
            return "Schwarzer Springer" ; }
        else {
            return "Weißer Springer";}
    }
}
