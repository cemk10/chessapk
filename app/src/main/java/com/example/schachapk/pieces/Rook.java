package com.example.schachapk.pieces;


import com.example.schachapk.common.Pair;

public class Rook  extends Piece {
    public Rook (Pair startPosition, PieceColor startColor) {
        super(startPosition, startColor);
    }
    public String getText (){
        if (getColor() == PieceColor.BLACK){
            return "Schwarze Türme"; }
        else {
            return "Weiße Türme";}
    }
}