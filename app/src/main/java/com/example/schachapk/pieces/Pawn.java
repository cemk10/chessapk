package com.example.schachapk.pieces;

import com.example.schachapk.common.Pair;

import java.util.ArrayList;
import java.util.List;

public class Pawn extends Piece {

    public Pawn(Pair startPosition, PieceColor startColor) {
        super(startPosition, startColor);
    }

    public String getText() {
        if (getColor() == PieceColor.BLACK) {
            return "Schwarzer Bauer";
        } else {
            return "Weißer Bauer";
        }
    }
    public List<Pair>getPossibleMoves(){
        List<Pair> possibleMoves = new ArrayList<>();

        int x = getPosition().getX();
        int y = getPosition().getY();

// Bewegung nach Vorne
        int forwardY = (getColor() == PieceColor.WHITE) ? y + 1:y -1;
        possibleMoves.add (new Pair(x, forwardY));

//Zwei Schritte vorwärts von der Startposition

        if (( getColor() ==PieceColor.WHITE && y ==1 ) || (getColor() == PieceColor.BLACK && y == 6 )) {
            int doubleForwardY = ( getColor() == PieceColor.WHITE) ? y +2 :y-2;
            possibleMoves.add(new Pair(x, doubleForwardY));

        }
        return possibleMoves;
    }
}


















