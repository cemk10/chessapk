package com.example.schachapk.pieces;

public enum PieceColor {
    WHITE,
    BLACK;
}
