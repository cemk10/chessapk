package com.example.schachapk

import android.os.Bundle
import androidx.activity.ComponentActivity
import com.example.schachapk.chessboard.Schachbrett

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(Schachbrett(applicationContext))
    }
}
