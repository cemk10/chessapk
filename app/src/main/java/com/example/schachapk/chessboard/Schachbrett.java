package com.example.schachapk.chessboard;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;
import android.view.View;
import com.example.schachapk.pieces.*;
import com.example.schachapk.common.Pair;

import java.util.ArrayList;
import java.util.List;

public class Schachbrett extends View { //View in Main implementieren
    private final Paint paint = new Paint();    //Paint Objekt erstellt
    private final List <Piece> pieces;

    PieceColor currentPlayer ;

    public Schachbrett(Context context) {
        super(context);
        paint.setColor(Color.BLACK);  // auf paint Objekt wird zugegriffen Farbe com.example.schachapk.chessboard.Schachbrett Schwarz
        paint.setStyle(Paint.Style.FILL);  // ausgefüllte Fläche Style
        pieces = new ArrayList<>();
        pieces.add(new Rook(Pair.create(0, 0), PieceColor.BLACK));
        pieces.add(new Rook(Pair.create(0, 7), PieceColor.WHITE));
        pieces.add(new Rook(Pair.create(7, 0), PieceColor.BLACK));
        pieces.add(new Rook(Pair.create(7, 7), PieceColor.WHITE));

        pieces.add(new Knight(Pair.create(1, 0), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(1, 7), PieceColor.WHITE));
        pieces.add(new Knight(Pair.create(6, 0), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(6, 7), PieceColor.WHITE));

        pieces.add(new Bishop(Pair.create(2, 0), PieceColor.BLACK));
        pieces.add(new Bishop(Pair.create(2, 7), PieceColor.WHITE));
        pieces.add(new Bishop(Pair.create(5, 7), PieceColor.BLACK));
        pieces.add(new Bishop(Pair.create(5, 0), PieceColor.WHITE));

        pieces.add(new Queen(Pair.create(3, 0), PieceColor.BLACK));
        pieces.add(new Rook(Pair.create(3, 7), PieceColor.WHITE));

        pieces.add(new King(Pair.create(4, 0), PieceColor.WHITE));
        pieces.add(new King(Pair.create(4, 7), PieceColor.WHITE));

        pieces.add(new Knight(Pair.create(0, 1), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(0, 6), PieceColor.WHITE));
        pieces.add(new Knight(Pair.create(1, 1), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(1, 6), PieceColor.WHITE));
        pieces.add(new Knight(Pair.create(2, 1), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(2, 6), PieceColor.WHITE));
        pieces.add(new Knight(Pair.create(3, 1), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(3, 6), PieceColor.WHITE));
        pieces.add(new Knight(Pair.create(4, 1), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(4, 6), PieceColor.WHITE));
        pieces.add(new Knight(Pair.create(5, 1), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(5, 6), PieceColor.WHITE));
        pieces.add(new Knight(Pair.create(6, 1), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(6, 6), PieceColor.WHITE));
        pieces.add(new Knight(Pair.create(7, 1), PieceColor.BLACK));
        pieces.add(new Knight(Pair.create(7, 6), PieceColor.WHITE));

        currentPlayer = PieceColor.WHITE;

    }
    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int width = getWidth();    //Breite
        int height = getHeight();   //Höhe
        int squareSize = width / 8;  //Quadrat größe

        for (int i = 0; i < 8; i++) {   //Spalten
            for (int j = 0; j < 8; j++) {  //Zeilen
                if ((i + j) % 2 == 0) {
                    paint.setColor(Color.WHITE);
                } else {
                    paint.setColor(Color.BLACK);
                }
                // Ausführen Zeichnung mit werten aus for Schleife
                canvas.drawRect(i * squareSize, j * squareSize, (i + 1) * squareSize, (j + 1) * squareSize, paint);

            }

        }
        for (int i = 0; i < pieces.size(); i++) {

            int x = squareSize * pieces.get(i).getPosition().getX();
            int y = squareSize * pieces.get(i).getPosition().getY() + squareSize / 2;

            Paint piecepaint = new Paint();
            piecepaint.setColor(Color.RED);
            piecepaint.setTextSize(16);
            canvas.drawText(pieces.get(i).getText(), x, y, piecepaint);
        }



    }
    public void updateCurrentPlayer(){

        if (currentPlayer == PieceColor.WHITE) {
            currentPlayer = PieceColor.BLACK;
             }
        else if (currentPlayer == PieceColor.BLACK) {
            currentPlayer = PieceColor.WHITE;
        }
    }

}
