package com.example.schachapk.common;

public class Pair {

    int x;

    int y;

    public Pair(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public static Pair create(int x, int y) {
        return new Pair(x, y);
    }

}
