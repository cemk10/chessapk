package com.example.schachapk.pieces;

import com.example.schachapk.common.Pair;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class PawnTest {
    @Test
    public void shouldReturnCorrectly(){
        Pawn pawn = new Pawn(Pair.create(0,1),PieceColor.WHITE);  // start bei 0,1 Korrekt
        List <Pair>result =  pawn.getPossibleMoves();  //Methode aufrufen
        assertEquals(2,result.size());                         // 2 Ergebnise erwartet verleich mit tatsächliche Größe
        assertEquals(0,result.get(0).getX());         // x soll 0 , x = 0  1. 0/0
        assertEquals(0,result.get(2).getY());        // y soll 0 , x = 2
        assertEquals(1,result.get(0).getX());         // x soll 1 , x = 0 2.1/1
        assertEquals(1,result.get(3).getY());        // y soll 1, y = 3
    }
}
